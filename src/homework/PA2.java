package cse132a.src.homework;

/**
 * 
 * @author Kevin L. Einstein
 *
 */

import java.sql.*;

public class PA2 {
	public static void main(String[] args) {
		Connection conn = null;
		try {
			Class.forName("org.sqlite.JDBC");
			conn = DriverManager.getConnection("jdbc:sqlite:C:/Users/User/workspace/CSE132aPA2/src/pa2.db");
			System.out.println("Opened database successfully.");

			Statement stmt = conn.createStatement();
			
			//--- create the delta
//			stmt.executeUpdate("CREATE TEMP TABLE t AS SELECT * FROM Flight");
//			stmt.executeUpdate("CREATE TEMP TABLE delta AS SELECT * FROM Flight");
//			stmt.executeUpdate("CREATE TEMP TABLE t_old AS SELECT * FROM t");
			//--- The set T is G
			stmt.executeUpdate("CREATE TEMP TABLE G (airline char(32), origin char(32), destination char(32), stops int DEFAULT 0)");
			stmt.executeUpdate("INSERT INTO G (airline, origin, destination) SELECT * FROM Flight");
			stmt.executeUpdate("DROP TABLE IF EXISTS Connected");
			stmt.executeUpdate("CREATE TABLE Connected AS SELECT * FROM G");
			stmt.executeUpdate("CREATE TEMP TABLE delta AS SELECT * FROM G");
			stmt.executeUpdate("CREATE TEMP TABLE t_old AS SELECT * FROM Connected");

			ResultSet delta = stmt.executeQuery("SELECT * FROM delta");
			int i = 0;
			while(delta.next()){
				stmt.executeUpdate("DELETE FROM t_old");
				stmt.executeUpdate("INSERT INTO t_old SELECT * FROM Connected");
				
				stmt.executeUpdate("DELETE FROM Connected");
				stmt.executeUpdate("INSERT INTO Connected SELECT * FROM t_old UNION SELECT x.airline, x.origin, y.destination, (x.stops + y.stops + 1) FROM t_old x, t_old y WHERE x.destination = y.origin AND x.airline = y.airline AND x.origin <> y.destination");

				delta.close(); // close the resultset and make a new one
				stmt.executeUpdate("DELETE FROM delta");
				stmt.executeUpdate("INSERT INTO delta SELECT airline, origin, destination, 0 FROM Connected EXCEPT SELECT airline, origin, destination, 0 FROM t_old");
				delta = stmt.executeQuery("SELECT * FROM delta");
				System.out.println("Iterations: " + i);
				i++;
			}
			System.out.println("Iterations: " + i);
			
			i = 0;
			stmt.executeUpdate("CREATE TEMP TABLE temp AS SELECT * FROM (SELECT airline, origin, destination, stops FROM Connected ORDER BY stops DESC) GROUP BY origin, destination, airline");
			stmt.executeUpdate("DELETE FROM Connected");
			stmt.executeUpdate("INSERT INTO Connected SELECT * FROM temp");
//			ResultSet output = stmt.executeQuery("SELECT * FROM (SELECT airline, origin, destination, stops FROM Connected ORDER BY stops DESC) GROUP BY origin, destination, airline");
			ResultSet output = stmt.executeQuery("SELECT * FROM Connected");
			while(output.next()){
				System.out.println(output.getString("airline") + ", " + output.getString("origin") + ", " + output.getString("destination") + ", " + output.getInt("stops"));
				i++;
			}
			System.out.println("Total Tuples: " + i);
			
			//--- delete the deltas
		} catch (Exception e) {
			throw new RuntimeException("There was a runtime problem!", e);
		} finally {
			try {
				if (conn != null)
					conn.close();
				System.out.println("Connection closed");
			} catch (SQLException e) {
				throw new RuntimeException("Cannot close the connection!", e);
			}
		}
		System.out.println("Program exit.");
	}
}// end class definition

// --- EOD ---//
